package com.cursospring.batch.jobscheduler.job;

import com.cursospring.batch.jobscheduler.tasklet.AgeGroupSummary;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.cursospring.batch.jobscheduler.utils.Constants.QUALIFIER_NAME;
import static com.cursospring.batch.jobscheduler.utils.Constants.STEP_NAME;

@Configuration
@AllArgsConstructor
public class DemoJob {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    @Qualifier(value = QUALIFIER_NAME)
    @Bean
    public Job demoOneJob() throws Exception {
        return jobBuilderFactory.get(QUALIFIER_NAME).start(stepOneDemo()).build();
    }

    private Step stepOneDemo() {
        return stepBuilderFactory.get(STEP_NAME).tasklet(new AgeGroupSummary()).build();
    }
}
