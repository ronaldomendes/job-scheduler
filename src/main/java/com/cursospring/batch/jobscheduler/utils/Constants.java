package com.cursospring.batch.jobscheduler.utils;

public class Constants {

    public static final String CONTEXT_KEY_NAME = "fileName";
    public static final String FILE_NAME_CSV = "employees.csv";
    public static final String FILE_PATH = "src/main/resources/employees.csv";
    public static final String QUALIFIER_NAME = "demoJob";
    public static final String STEP_NAME = "stepOne";
}
