# Spring Batch: Scheduled Job

In this project you will learn a way of using *@Scheduled* annotation.

With this resource you are able to process a task without using web library.

This example also use a Tasklet sample showing the advantages of Spring Batch architecture.